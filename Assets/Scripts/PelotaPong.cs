using UnityEngine;
using UnityEngine.UI;

public class PelotaPong : MonoBehaviour
{
    public GameManager gameManager;

    public float fuerza = 300; // fuerza de la pelota (revisar inspector)

    int puntuacionJ1 = 0; // Puntuaci�n del jugador 1
    int puntuacionJ2 = 0; // Puntuaci�n del jugador 2

    public Text puntosJ1; // Texto de la UI para los puntos del J1
    public Text puntosJ2; // Texto de la UI para los puntos del J2

    public AudioClip gol;   // Clip de audio para el gol
    public AudioClip rebote;//Clip de audio para el rebote

    /// <summary>
    /// Revisa si se ha pulsado el espacio para resetear la pelota
    /// </summary>
    private void Update()
    {

        bool enJuego = gameManager.EnJuego();

        if (enJuego == true)
        {
            // Comprobamos si se ha pulsado espacio
            if (Input.GetButtonDown("Jump"))
            {
                ResetearPelota();
                Sacar();
            }
        }
    }

    /// <summary>
    /// Controla las colisiones para reproducir sonidos y marcar goles
    /// </summary>
    /// <param name="other">El objeto contra el que ha colisionado la bola</param>
    private void OnCollisionEnter(Collision other)
    {

        bool golMarcado = false; // Variable de control para ver si se ha marcado un gol

        if (other.gameObject.tag == "Izq") // Comprobamos si la pelota ha chocado con la pared izquierda
        {
            ResetearPelota();
            SumarPunto("J2");
            MostrarPuntuacionConsola();
            ReproducirClip(gol);

            golMarcado = true; // Anotamos que se ha marcado un gol
        }
        if (other.gameObject.tag == "Der")
        {
            ResetearPelota();
            SumarPunto("J1");
            MostrarPuntuacionConsola();
            ReproducirClip(gol);

            golMarcado = true;
        }

        // Si se ha marcado un gol
        if (golMarcado == false)
        {
            ReproducirClip(rebote);
        }
    }

    /// <summary>
    /// Resetea la posici�n de la pelota
    /// </summary>
    void ResetearPelota()
    {
        GetComponent<Rigidbody>().velocity = Vector3.zero;   // Resetea la velocidad
        transform.position = new Vector3(0, 0, 0);           // Resetea la posici�n
    }

    /// <summary>
    /// Pone en juego la pelota
    /// </summary>
    void Sacar()
    {
        Vector3 angulo = CalcularAnguloSaque();                 // Calculamos el �ngulo de saque y lo guardamos en una variable
        GetComponent<Rigidbody>().AddForce(angulo * fuerza);    // A�adimos una fuerza para poner la pelota en juego
    }

    /// <summary>
    /// Este m�todo calcula el �ngulo de saque, impidiendo que la pelota salga con poco �ngulo y rebote demasiado antes de llegar a la pala.
    /// </summary>
    /// <returns>La direcci�n de la pelota</returns>
    private Vector3 CalcularAnguloSaque()
    {
        // Obtenemos dos variables aleatorias
        float aleatorioX = Random.Range(-1F, 1F);
        float aleatorioY = Random.Range(-1F, 1F);

        // En el raro caso de que X sea 0, la cambiamos a positivo
        if (aleatorioX == 0) { 
            aleatorioX = 1; 
        }

        // No queremos que X tenga decimales para que no haya �ngulos cerrados en el saque
        if (aleatorioX > 0)
        {
            aleatorioX = 1;
        }
        if (aleatorioX < 0)
        {
            aleatorioX = -1;
        }

        // Obtenemos un vector de direcci�n que siempre va a tener un �ngulo m�nimo en X
        Vector3 direccion = new Vector3(aleatorioX, aleatorioY, 0);

        // Devolvemos el vector de direcci�n normalizado
        return direccion.normalized;
    }

    /// <summary>
    /// Reproduce un clip de audio
    /// </summary>
    /// <param name="clip">El clip para reproducir</param>
    void ReproducirClip(AudioClip clip)
    {
        GetComponent<AudioSource>().clip = clip; // Cambiamos el clip del AudioSource
        GetComponent<AudioSource>().Play();      // Reproducimos el clip
    }

    /// <summary>
    /// Muestra la puntuaci�n por la consola
    /// </summary>
    void MostrarPuntuacionConsola()
    {
        print("LA PUNTUACI�N ES: " + puntuacionJ1 + " - " + puntuacionJ2);
    }

    /// <summary>
    /// Suma Puntos al jugador
    /// </summary>
    /// <param name="jugador">Jugador que ha marcado el gol</param>
    void SumarPunto(string jugador)
    {
        if(jugador == "J1"){
            puntuacionJ1 += 1;                          // Sumamos 1 a la variable de puntuaci�n
            puntosJ1.text = puntuacionJ1.ToString();    // Actualizamos la UI
        }
        else
        {
            puntuacionJ2 += 1;
            puntosJ2.text = puntuacionJ2.ToString();
        }
    }
}
