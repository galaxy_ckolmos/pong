using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public GameObject panelMenu;
    public PalaPong palaJ2;

    bool jugando = false;

    public bool EnJuego()
    {
        return jugando;
    }

    public void Jugador_vs_Jugador()
    {
        ComenzarJuego();
        palaJ2.Humanificar(true);
    }

    public void Jugador_vs_IA()
    {
        ComenzarJuego();
        palaJ2.Humanificar(false);
    }

    void ComenzarJuego()
    {
        jugando = true;
        panelMenu.SetActive(false);
    }
}
